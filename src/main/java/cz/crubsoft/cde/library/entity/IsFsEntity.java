package cz.crubsoft.cde.library.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "is_fs", schema = "katalog3", catalog = "")
public class IsFsEntity {
    @Id
    @Column(name = "IS_ID")
    private int isId;
    @Basic
    @Column(name = "ZKRATKA")
    private String zkratka;
    @Basic
    @Column(name = "NAZEV")
    private String nazev;

    public int getIsId() {
        return isId;
    }

    public void setIsId(int isId) {
        this.isId = isId;
    }

    public String getZkratka() {
        return zkratka;
    }

    public void setZkratka(String zkratka) {
        this.zkratka = zkratka;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IsFsEntity that = (IsFsEntity) o;

        if (isId != that.isId) return false;
        if (zkratka != null ? !zkratka.equals(that.zkratka) : that.zkratka != null) return false;
        if (nazev != null ? !nazev.equals(that.nazev) : that.nazev != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = isId;
        result = 31 * result + (zkratka != null ? zkratka.hashCode() : 0);
        result = 31 * result + (nazev != null ? nazev.hashCode() : 0);
        return result;
    }
}
